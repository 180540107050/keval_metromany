package com.keval.matrimony.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserCityModel implements Serializable {

    int UserCityId;
    String Name;
}
