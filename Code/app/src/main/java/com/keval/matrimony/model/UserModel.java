package com.keval.matrimony.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserModel implements Serializable {

    int UserId;
    String UserName;
    String UserFatherName;
    String UserSurname;
    int UserGender;
    String UserHobbies;
    String UserDob;
    String UserEmailAddress;
    String PhoneNumber;
    int UserLanguageId;
    int UserCityId;
    String Language;
    String City;
    int IsFavorite;
    String Age;
    String UserDobTime;
}
