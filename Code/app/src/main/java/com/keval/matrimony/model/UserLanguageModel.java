package com.keval.matrimony.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserLanguageModel implements Serializable {
    int UserLanguageId;
    String name;
}
